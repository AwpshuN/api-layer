const express = require('express');
const routes = require('./routes/influxDB.route');
const influxConn = require('../api-layer/config')
const cors = require('cors');


const app = express();

app.use(express.json());
app.use(cors());

app.get('/api', (req, res) => {
    res.send("hello world");
});

const port = process.env.port || 3000;

app.use('/api', routes);

app.listen(port, () => {
    console.log(`Server is running on ${port}`);
});