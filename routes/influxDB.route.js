const Router = require("express").Router();
const controller = require("../controller/influxDB.controller");
const { validationResult, check } = require("express-validator");
const validator = require("../middleware/validator.middleware");

Router.get(
  "/getCountofSensor",
  check("device").isString(),
  check("tag").isString(),
  check("sTime").isNumeric(),
  check("eTime").isNumeric(),
  validator,
  (req, res) => {
    controller
      .getCountofSensor(
        req.query.device,
        req.query.tag,
        req.query.sTime,
        req.query.eTime
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((error) => {
        console.log(error);
        res.status(400).send({ sucess: false });
      });
  }
);

Router.get(
  "/getDataofSensor",
  check("device").isString(),
  check("tag").isString(),
  check("sTime").isNumeric(),
  check("eTime").isNumeric(),
  validator,
  (req, res) => {
    controller
      .getDataofSensor(
        req.query.device,
        req.query.tag,
        req.query.sTime,
        req.query.eTime
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((error) => {
        console.log(error);
        res.status(400).send({ sucess: false });
      });
  }
);

Router.get(
  "/getDataByStEt",
  check("device").isString(),
  check("sTime").isNumeric(),
  check("eTime").isNumeric(),
  validator,
  (req, res) => {
    controller
      .getDataByStEt(req.query.device, req.query.sTime, req.query.eTime)
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((error) => {
        console.log(error);
        res.status(400).send({ sucess: false });
      });
  }
);

Router.get(
  "/getHourlyDpSensor",
  check("device").isString(),
  check("tag").isString(),
  check("sTime").isNumeric(),
  check("eTime").isNumeric(),
  validator,
  (req, res) => {
    controller
      .getDPAfterButBefore(
        req.query.device,
        req.query.tag,
        req.query.sTime,
        req.query.eTime
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((error) => {
        console.log(error);
        res.status(400).send({ sucess: false });
      });
  }
);

Router.get(
  "/getAllData",
  check("device").isString(),
  check("tag").isString(),
  check("sTime").isNumeric(),
  check("eTime").isNumeric(),
  validator,
  (req, res) => {
    controller
      .getAllData(
        req.query.device,
        req.query.tag,
        req.query.sTime,
        req.query.eTime
      )
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((error) => {
        console.log(error);
        res.status(400).send({ sucess: false });
      });
  }
);

module.exports = Router;
