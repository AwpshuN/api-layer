const Influx = require('influx');

const influxConn = new Influx.InfluxDB("http://localhost:8086/statistics");

influxConn.getDatabaseNames().then((names) => {
    if (!names.includes("statistics")) {
      return influxConn.createDatabase("statistics");
    }
  });

  module.exports = {influxConn};
  