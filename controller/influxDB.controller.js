const { influxConn } = require("../config");

const getCountofSensor = async (device, tag, sTime, eTime) => {
  const query = `SELECT COUNT(value) FROM ${device} WHERE "tag" = '${tag}' AND "time" >= ${sTime} AND "time" <= ${eTime}`;
  const data = await influxConn.query(query);

  return data;
};

const getDataofSensor = async (device, tag, sTime, eTime) => {
  const query = `SELECT "value" FROM ${device} WHERE "tag" = '${tag}' AND "time" >= ${sTime} AND "time" <= ${eTime}`;
  const data = await influxConn.query(query);

  return data;
};

const getDataByStEt = async (device, sTime, eTime) => {
  const query = `SELECT * FROM ${device} WHERE "time" >= ${sTime} AND "time" <= ${eTime}`;
  const data = influxConn.query(query);

  return data;
};

const getDPAfterButBefore = async (device, tag, sTime, eTime) => {
  const query = `SELECT * FROM ${device} WHERE "tag" = '${tag}' AND time >= ${sTime} AND time <= ${eTime} ORDER BY time ASC LIMIT 1`;
  const data = influxConn.query(query);

  return data;
};

const getAllData = async (device, tag, sTime, eTime) => {
  const query = `SELECT * FROM ${device} WHERE "tag" = '${tag}' AND time >= ${sTime} AND time <= ${eTime}`;
  const data = influxConn.query(query);

  return data;
};

module.exports = {
  getCountofSensor,
  getDataofSensor,
  getDataByStEt,
  getDPAfterButBefore,
  getAllData,
};
